

#include <SDL2/SDL.h>
#include <signal.h>
#include <stdlib.h>
#include <unistd.h>

#include "game.h"
#include "graphics/graphics.h"
#include "utils/color.h"
#include "utils/bool.h"

int main(int argc, char const *argv[]) {
	SDL_Window *window;
	SDL_Renderer *renderer;

	if (argc != 3) {
		printf("Usage : ./out <IP> <PORT>\n");
		return 1;
	}

	/*if (signal(SIGPIPE, gameSignalHandler) == SIG_ERR) {
		printf(RED "[CLIENT] Can't catch signals\n" RESET);
	}*/

	config_t config = configNew(0, argv[1], atoi(argv[2]));

	gcsInit();
	SDL_DisplayMode DM;
	SDL_GetCurrentDisplayMode(0, &DM);
	gcsNewWindow("Maze Ghost", DM.w/2, DM.h/2, &window);
	gcsNewRenderer(window, &renderer);

    bool launch_game = gameTitle(window);

    if (launch_game)
    {
        gameSetup(config);
        gameStart(window);
    }

	gcsFreeRenderer(&renderer);
	gcsFreeWindow(&window);
	gcsFree();

	return 0;
}
