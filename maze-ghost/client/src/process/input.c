#include "input.h"

#include "../game.h"
#include "../graphics/musicPlayer.h"
#include "../utils/app.h"

bool windowed = true;

void inputPlayerMovement(bool *keyPressed, direction_t *direction) {
	const Uint8 *keystates = SDL_GetKeyboardState(NULL);
	*keyPressed = false;
	if (keystates[KEY_UP]) {
		*direction = DIR_UP;
		*keyPressed = true;
	}
	if (keystates[KEY_DOWN]) {
		*direction = DIR_DOWN;
		*keyPressed = true;
	}
	if (keystates[KEY_LEFT]) {
		*direction = DIR_LEFT;
		*keyPressed = true;
	}
	if (keystates[KEY_RIGHT]) {
		*direction = DIR_RIGHT;
		*keyPressed = true;
	}
}

void inputMisc(bool *halt, bool *running, camera_t *camera, player_t *player, SDL_Window *window, int *windowWidth, int *windowHeight) {
	SDL_Event event;

	while (SDL_PollEvent(&event)) {
		switch (event.type) {
		case SDL_WINDOWEVENT:
			switch (event.window.event) {
			case SDL_WINDOWEVENT_CLOSE:
				*running = false;
				break;
			case SDL_WINDOWEVENT_SIZE_CHANGED:
				gameRenderGame(window);
				SDL_GetWindowSize(window, windowWidth, windowHeight);
				break;
			}
			break;
		case SDL_QUIT:
			*running = false;
			break;
		case SDL_KEYDOWN:
			switch (event.key.keysym.sym) {
			case KEY_PAUSE:
				*halt = !(*halt);
				break;
			case KEY_QUIT:
				*running = false;
				break;
			case KEY_DEBUG:
				cameraToggleDebug(camera);
				break;
			case KEY_HITBOXS:
				cameraToggleHitboxs(camera);
				break;
			case KEY_FULLSCREEN:
				SDL_SetWindowFullscreen(window, windowed ? SDL_WINDOW_FULLSCREEN_DESKTOP : 0);
				windowed = !windowed;
				break;
			case KEY_VOLUME_UP:
				musicVolumeUp();
				break;
			case KEY_VOLUME_DOWN:
				musicVolumeDown();
				break;
			case KEY_VOLUME_MUTE:
				musicVolumeMute();
				break;
			case KEY_SUPER_POWER:
				playerUseSpecialPower(player);
				break;
			}
			break;
		}
	}
}
