#ifndef GAME
#define GAME

#include "utils/config.h"
#include "utils/bool.h"

#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>

#define MS_SLEEP_WANTED 16

#define PATH_TITLE_SCREEN "textures/player/player0.png"


void gameSetup(config_t config);
void gameStart(SDL_Window *window);
void gameRenderGame(SDL_Window *window);
void gameEnd(SDL_Window *window);
void gameSignalHandler(int signo);
bool gameTitle(SDL_Window * window);

#endif
