#ifndef GRAPH
#define GRAPH

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "partition.h"

typedef struct graphe {
	int nbNoeuds;
	int *i;
	int *j;
	int nbArretes;
} graphe_t;

graphe_t *grapheInit(int size);

void graphArreteInit(graphe_t *G, int i, int j);

graphe_t *graphCreer(int size);

graphe_t *graphCreerPourLaby(int size);

void graphLiberer(graphe_t **G);

partition_t graphConnexes(graphe_t *G);

void graphDisplay(graphe_t *G);

void graphDebug(graphe_t *G);

graphe_t *kruskal(graphe_t *g, void (*f)(graphe_t *), double proba);

void fisherYates(graphe_t *G);

void graphDeleteWalls(graphe_t *G, int s);

void graphCreateRoom(graphe_t *G, int numberOfRooms);



#endif
