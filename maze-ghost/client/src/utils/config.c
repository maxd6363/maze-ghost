#include "config.h"

#include <stdio.h>
#include <string.h>

config_t configNew(int worldSize, const char *server, int port) {
	config_t config;
	config.worldSize = worldSize;
	config.port = port;
	strcpy(config.server, server);
	return config;
}

void configPrint(config_t config) {
	printf("CONFIG :\n");
	printf("World size : %d\n", config.worldSize);
	printf("Server IP : %s\n", config.server);
	printf("Server port : %d\n", config.port);
}