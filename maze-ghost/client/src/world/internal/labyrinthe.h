#ifndef LABYRINTHE_H_
#define LABYRINTHE_H_

#include "../../graph/graph.h"
#include "../../utils/direction.h"
#include "tile.h"

#define SCALE 20

typedef struct {
	int size;
	tile_t **data;
} labyrinthe_t;

labyrinthe_t labyrintheInit(graphe_t *graph);
void labyrintheUpdate(labyrinthe_t *out, graphe_t *graph);
void labyrintheFree(labyrinthe_t *laby);
void labyrintheDebug(labyrinthe_t *laby);
bool labyrintheAbleToGo(labyrinthe_t *laby, int fromX, int fromY, direction_t direction);
void labytintheDeserialize(labyrinthe_t *out, char *data);




#endif