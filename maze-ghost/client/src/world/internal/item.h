#ifndef ITEM
#define ITEM

#include "../../utils/bool.h"
#include "labyrinthe.h"

#define ITEM_WIDTH 50
#define ITEM_HEIGHT 50
#define MAX_ITEMS 5


typedef struct {
  float x;
  float y;
  int w;
  int h;
  int xWorldPosition;
  int yWorldPosition;
  bool exist;
} item_t;


void itemInit (item_t *out, int xWorld, int yWorld, int tileSize);
void itemsList (item_t * items, labyrinthe_t * laby, int tileSize);
void itemsCopyFromThread(item_t *dest,item_t *src);

#endif
