#include "camera.h"

#include <pthread.h>
#include <unistd.h>

void cameraInit(camera_t *out, int worldSize, int playerID, int windowSize) {
	if (out) {
		if (playerID == 0) {
			out->scale = ((float)windowSize) / (worldSize * CAMERA_DEFAULT_TILESIZE);
		} else {
			out->scale = 1;
		}
		out->viewport.x = 0;
		out->viewport.y = 0;
		out->viewport.w = worldSize * CAMERA_DEFAULT_TILESIZE;
		out->viewport.h = worldSize * CAMERA_DEFAULT_TILESIZE;
		out->tileSize = CAMERA_DEFAULT_TILESIZE;
		out->renderHitbox = false;
		out->renderDebug = false;
	}
}

void cameraUpdate(camera_t *out, SDL_Renderer *renderer, player_t *player, int worldSize, int windowWidth, int windowHeight) {
	if (out) {
		if (player->id != 0) {
			out->viewport.x = -player->x + windowWidth / 2 - PLAYER_WIDTH / 2;
			out->viewport.y = -player->y + windowHeight / 2 - PLAYER_HEIGHT / 2;
		} else {
			out->scale = ((float)windowHeight) / (worldSize * CAMERA_DEFAULT_TILESIZE);
			out->viewport.x = (windowWidth - windowHeight)*1.5;
		}
	}
	SDL_RenderSetScale(renderer, out->scale, out->scale);
	SDL_RenderSetViewport(renderer, &out->viewport);
}

void cameraToggleDebug(camera_t *out) {
	if (out) {
		out->renderDebug = !out->renderDebug;
	}
}

void cameraToggleHitboxs(camera_t *out) {
	if (out) {
		out->renderHitbox = !out->renderHitbox;
	}
}

void cameraZoomIn(camera_t *out) {
	if (out) {
		out->scale += CAMERA_ZOOM_STEP;
	}
}

void cameraZoomOut(camera_t *out) {
	if (out) {
		out->scale -= CAMERA_ZOOM_STEP;
	}
}

void *cameraZoomInAnimatedInternal(camera_t *out) {
	int frame = 0;
	while (frame < CAMERA_ZOOM_SCALE) {
		cameraZoomIn(out);
		usleep(CAMERA_ZOOM_SLEEP_MICROSEC);
		frame++;
	}
	return NULL;
}

void *cameraZoomOutAnimatedInternal(camera_t *out) {
	int frame = 0;
	while (frame < CAMERA_ZOOM_SCALE) {
		cameraZoomOut(out);
		usleep(CAMERA_ZOOM_SLEEP_MICROSEC);
		frame++;
	}
	return NULL;
}

void cameraZoomInAnimated(camera_t *out) {
	pthread_t threadAnimation;
	pthread_create(&threadAnimation, NULL, (void *(*)(void *))cameraZoomInAnimatedInternal, (void *)out);
}

void cameraZoomOutAnimated(camera_t *out) {
	pthread_t threadAnimation;
	pthread_create(&threadAnimation, NULL, (void *(*)(void *))cameraZoomOutAnimatedInternal, (void *)out);
}