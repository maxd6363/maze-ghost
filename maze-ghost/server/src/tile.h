#ifndef TILE_H_
#define TILE_H_

#define BYTE_TO_BINARY_PATTERN "%c%c%c%c%c%c%c%c"
#define BYTE_TO_BINARY(byte)  \
  (byte & 0x80 ? '1' : '0'), \
  (byte & 0x40 ? '1' : '0'), \
  (byte & 0x20 ? '1' : '0'), \
  (byte & 0x10 ? '1' : '0'), \
  (byte & 0x08 ? '1' : '0'), \
  (byte & 0x04 ? '1' : '0'), \
  (byte & 0x02 ? '1' : '0'), \
  (byte & 0x01 ? '1' : '0') 

#include "utils/bool.h"

typedef enum {
	TILE_OPENED =       0b00000001,
	TILE_CLOSED_DOWN =  0b00000010,
	TILE_CLOSED_UP =    0b00000100,
	TILE_CLOSED_LEFT =  0b00001000,
	TILE_CLOSED_RIGHT = 0b00010000,
	TILE_CLOSED = TILE_CLOSED_DOWN | TILE_CLOSED_UP | TILE_CLOSED_LEFT | TILE_CLOSED_RIGHT
} tile_t;

void tileDebug(tile_t tile);

bool isTile(tile_t toTest, tile_t value);
void tileRemoveFlags(tile_t *toModify, tile_t value);

#endif