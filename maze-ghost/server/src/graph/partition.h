#ifndef partition_h
#define partition_h

#include "../utils/bool.h"

typedef int element_t;
typedef int identifier_t;

#define BAD_IDENTIFIER -1

//#define IMAGE_VIEWER "display"
#define IMAGE_VIEWER "eog"

#define IMAGE_FORMAT "svg"
//#define IMAGE_FORMAT "png"




typedef struct partition {
	int size;
	element_t *data;
	bool *class; // vrai si index est un identifiant de classe, faux sinon
	int *heights;
} partition_t;

/**
 * @brief Crée la partition où chaque élément est seul dans sa classe
 * 
 * @param size Le nombre d'éléments
 * @return partition_t La partition
 */
partition_t partitionCreer(int size);


/**
 * @brief Libère la mémoire pour une partition
 * 
 * @param partition La partition
 */
void partitionLiberer(partition_t *partition);

/**
 * @brief Affiche une partition
 * 
 * @param partition La partition
 */
void partitionAfficher(partition_t *partition);


/**
 * @brief Renvoie un identifiant unique de la classe de x 
 * 
 * @param p La partition
 * @param x Un élément de la partition
 * @return identifier_t 
 */
identifier_t partitionRecupererClasse(partition_t *p, element_t x);

/**
 * @brief Fusionne les classes de x et y
 * 
 * @param p La partition en entrée sortie
 * @param x Un élément de la partition
 * @param y Un autre élément de la partition
 */
void partitionFusion(partition_t *p, element_t x, element_t y);

/**
 * @brief Liste les éléments de la classe
 * 
 * @param p La partition
 * @param id L'identifiant d'une classe
 * @return element_t* La liste des éléments de la classe
 */
element_t *partitionListerClasse(partition_t *p, identifier_t id);

/**
 * @brief Renvoie la liste des classes
 * 
 * @param p La partition
 * @return bool* la liste des identifiant de toutes les classes
 */
bool *partitionListerPartition(partition_t *p);


/**
 * @brief Afficher une partition de façon lisible
 * 
 * @param p La partition
 */
void partitionAfficherPretty(partition_t *p);


/**
 * @brief Exporte une partition en fichier .dot
 * 
 * @param p La partition
 * @param filename Le nom du fichier .dot
 */
void partitionToDotFile(partition_t *p, char filename[]);


/**
 * @brief Génère et affiche la partition à partir du .dot
 * 
 * @param filename Fichier .dot
 */
void partitionOpenImage(char filename[]);

/**
 * @brief Supprime les fichiers .dot et .svg
 * 
 */
void partitionCleanFiles(void);


/**
 * @brief Calcule le nombre de classes différentes dans la partition
 * 
 * @param p Partition
 * @return int Nombre de classes
 */
int partitionNumberOfClass(partition_t *p);



#endif
