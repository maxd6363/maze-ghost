#include "server.h"

#include <arpa/inet.h>
#include <math.h>
#include <netinet/tcp.h>
#include <pthread.h>
#include <signal.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <time.h>
#include <unistd.h>

#include "graph/graph.h"
#include "labyrinthe.h"
#include "player.h"
#include "utils/app.h"
#include "utils/bool.h"
#include "utils/color.h"
#include "utils/direction.h"

player_t players[MAX_CLIENTS];
bool items[MAX_ITEMS];

int numberOfPlayers = 0;
int serverSeed = 42;
double serverPercentKruskal;
int serverPort;
int serverNumberOfRooms;
int serverWorldSize;

char longerBuffer[LONG_BUFFER * (MAX_CLIENTS + 1)];
char longBuffer[LONG_BUFFER];
char shortBuffer[SHORT_BUFFER];

void serverSetup(int port, int worldSize, double percentKruskal, int numberOfRooms) {
	srand(serverSeed);
	serverPort = port;
	serverPercentKruskal = percentKruskal;
	serverNumberOfRooms = numberOfRooms;
	serverWorldSize = sqrt(worldSize);

	for (int i = 0; i < MAX_CLIENTS; i++) {
		players[i].id = i;
		players[i].connected = false;
	}
	for (int i = 0; i < MAX_ITEMS; i++) {
		items[i] = false;
	}

	serverSpeak(ok, "World size : %d", (int)sqrt(worldSize));
	serverSpeak(ok, "Percent kruskal : %d%%", (int)(percentKruskal * 100));
}

void serverStart(void) {
	int index = 0;
	int clientsSD[MAX_CLIENTS * 1000];
	pthread_t threadClient[MAX_CLIENTS * 1000];
	int sockfd;
	struct sockaddr_in serverAddress;
	bool running = true;

	sockfd = socket(AF_INET, SOCK_STREAM, 0);
	serverAddress.sin_family = AF_INET;
	serverAddress.sin_addr.s_addr = INADDR_ANY;
	serverAddress.sin_port = htons(serverPort);

	serverSpeak(ok, "Running on version : %s", APP_VERSION);
	serverSpeak(ok, "Started on port : %d", serverPort);

	if (bind(sockfd, (struct sockaddr *)&serverAddress, sizeof(serverAddress)) != 0) {
		serverSpeak(error, "Can't bind on port %d", serverPort);
		return;
	}

	listen(sockfd, MAX_CLIENTS);
	serverSpeak(ok, "Listening for clients");

	while (running) {
		clientsSD[index] = accept(sockfd, NULL, NULL);
		if (clientsSD[index] != 0) {
			numberOfPlayers++;
			pthread_create(&threadClient[index], NULL, (void *(*)(void *))handleClient, &clientsSD[index]);
			index++;
		}
	}
	serverSpeak(ok, "Shutdown");
}

void *handleClient(int *sd) {
	bool running = true;
	int threadID = 0;

	while (players[threadID].connected) {
		threadID++;
	}
	if (threadID >= MAX_CLIENTS) {
		threadSpeak(-1, error, "Not enough space for client");
	}

	for (int i = 0; i < MAX_CLIENTS; i++) {
		players[i].id = i;
	}

	threadSpeak(threadID, ok, "Started");

	running = sendAppVersion(*sd, threadID);
	running = sendLaby(*sd, threadID);
	running = sendPlayerInfo(*sd, threadID);

	while (running) {
		if (!getGameState(*sd, threadID))
			break;
		if (!sendGameState(*sd, threadID))
			break;
	}
	numberOfPlayers--;
	players[threadID].connected = false;
	players[threadID].id = threadID;
	threadSpeak(threadID, error, "Client disconnected");
	return NULL;
}

bool sendLaby(int sd, int idThread) {
	sprintf(longBuffer, "%d %d %.2lf %d\n", serverWorldSize, serverSeed, serverPercentKruskal, serverNumberOfRooms);
	if (send(sd, longBuffer, LONG_BUFFER, 0) != LONG_BUFFER) {
		threadSpeak(idThread, error, "Can't send maze");
		return false;
	}
	return true;
}

bool sendAppVersion(int sd, int idThread) {
	sprintf(shortBuffer, APP_VERSION);
	if (send(sd, shortBuffer, SHORT_BUFFER, 0) != SHORT_BUFFER) {
		threadSpeak(idThread, error, "Can't send app version");
		return false;
	}
	return true;
}

bool sendPlayerInfo(int sd, int idThread) {
	sprintf(longBuffer, "%d %d %d\n", idThread, rand() % serverWorldSize, rand() % serverWorldSize);
	if (send(sd, longBuffer, LONG_BUFFER, 0) != LONG_BUFFER) {
		threadSpeak(idThread, error, "Can't send player ID");
		return false;
	}
	return true;
}

bool sendGameState(int sd, int idThread) {
	char *curr = longerBuffer;
	for (int i = 0; i < MAX_CLIENTS; i++) {
		playerSerialize(&players[i], curr);
		curr += LONG_BUFFER;
	}
	sprintf(curr, "%d %d %d %d %d\n", items[0], items[1], items[2], items[3], items[4]);

	if (send(sd, longerBuffer, LONG_BUFFER * (MAX_CLIENTS + 1), 0) != LONG_BUFFER * (MAX_CLIENTS + 1)) {
		threadSpeak(idThread, error, "Can't send players positions");
	}
	return true;
}

bool getGameState(int sd, int idThread) {
	char *curr = longerBuffer;
	if (recv(sd, longerBuffer, LONG_BUFFER + MAX_ITEMS * 3, 0) != LONG_BUFFER + MAX_ITEMS * 3) {
		threadSpeak(idThread, error, "Can't get game state");
		return false;
	}
	playerDeserialize(&players[idThread], curr);
	curr += LONG_BUFFER;
	players[idThread].connected = true;
	sscanf(curr, "%d %d %d %d %d", (int *)&(items[0]), (int *)&(items[1]), (int *)&(items[2]), (int *)&(items[3]), (int *)&(items[4]));

	return true;
}

void signalHandler(int signo) {
	if (signo == SIGPIPE) {
		printf(BLU "[SERVER] Client disconnected\n" RESET);
	}
}

void serverSpeak(message_type_t type, const char *message, ...) {
	va_list args;
	va_start(args, message);
	switch (type) {
	case ok:
		printf(GRE);
		break;
	case error:
		printf(RED);
		break;
	case warning:
		printf(YEL);
		break;
	}
	printf("[SERVER] ");
	vprintf(message, args);
	printf("\n" RESET);
	va_end(args);
}

void threadSpeak(int id, message_type_t type, const char *message, ...) {
	va_list args;
	va_start(args, message);
	switch (type) {
	case ok:
		printf(GRE);
		break;
	case error:
		printf(RED);
		break;
	case warning:
		printf(YEL);
		break;
	}
	printf("[THREAD %d] ", id);
	vprintf(message, args);
	printf("\n" RESET);
	va_end(args);
}